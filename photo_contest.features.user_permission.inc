<?php
/**
 * @file
 * photo_contest.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function photo_contest_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create photo_contest content'.
  $permissions['create photo_contest content'] = array(
    'name' => 'create photo_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create photo_contest_entry content'.
  $permissions['create photo_contest_entry content'] = array(
    'name' => 'create photo_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any photo_contest content'.
  $permissions['delete any photo_contest content'] = array(
    'name' => 'delete any photo_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any photo_contest_entry content'.
  $permissions['delete any photo_contest_entry content'] = array(
    'name' => 'delete any photo_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own photo_contest content'.
  $permissions['delete own photo_contest content'] = array(
    'name' => 'delete own photo_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own photo_contest_entry content'.
  $permissions['delete own photo_contest_entry content'] = array(
    'name' => 'delete own photo_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any photo_contest content'.
  $permissions['edit any photo_contest content'] = array(
    'name' => 'edit any photo_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any photo_contest_entry content'.
  $permissions['edit any photo_contest_entry content'] = array(
    'name' => 'edit any photo_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own photo_contest content'.
  $permissions['edit own photo_contest content'] = array(
    'name' => 'edit own photo_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own photo_contest_entry content'.
  $permissions['edit own photo_contest_entry content'] = array(
    'name' => 'edit own photo_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag enter_photo_in_contest'.
  $permissions['flag enter_photo_in_contest'] = array(
    'name' => 'flag enter_photo_in_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'publish button publish any photo_contest'.
  $permissions['publish button publish any photo_contest'] = array(
    'name' => 'publish button publish any photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish any photo_contest_entry'.
  $permissions['publish button publish any photo_contest_entry'] = array(
    'name' => 'publish button publish any photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable photo_contest'.
  $permissions['publish button publish editable photo_contest'] = array(
    'name' => 'publish button publish editable photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable photo_contest_entry'.
  $permissions['publish button publish editable photo_contest_entry'] = array(
    'name' => 'publish button publish editable photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own photo_contest'.
  $permissions['publish button publish own photo_contest'] = array(
    'name' => 'publish button publish own photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own photo_contest_entry'.
  $permissions['publish button publish own photo_contest_entry'] = array(
    'name' => 'publish button publish own photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any photo_contest'.
  $permissions['publish button unpublish any photo_contest'] = array(
    'name' => 'publish button unpublish any photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any photo_contest_entry'.
  $permissions['publish button unpublish any photo_contest_entry'] = array(
    'name' => 'publish button unpublish any photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable photo_contest'.
  $permissions['publish button unpublish editable photo_contest'] = array(
    'name' => 'publish button unpublish editable photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable photo_contest_entry'.
  $permissions['publish button unpublish editable photo_contest_entry'] = array(
    'name' => 'publish button unpublish editable photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own photo_contest'.
  $permissions['publish button unpublish own photo_contest'] = array(
    'name' => 'publish button unpublish own photo_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own photo_contest_entry'.
  $permissions['publish button unpublish own photo_contest_entry'] = array(
    'name' => 'publish button unpublish own photo_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'unflag enter_photo_in_contest'.
  $permissions['unflag enter_photo_in_contest'] = array(
    'name' => 'unflag enter_photo_in_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'directory user' => 'directory user',
      'editor' => 'editor',
      'forum monitor' => 'forum monitor',
      'registered user' => 'registered user',
      'retailer' => 'retailer',
      'site admin' => 'site admin',
      'writer' => 'writer',
    ),
    'module' => 'flag',
  );

  return $permissions;
}
